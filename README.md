# Template for Remark

[Remark: A simple, in-browser, markdown-driven slideshow tool.](https://github.com/gnab/remark)

[Backslide: CLI tool for building Remark presentations](https://github.com/sinedied/backslide)

# Styles and scripts

TODO

scale?
remove the horrid cirlce fade?
background image/logo?


# Usage

The style.scss and scripts in index.html can be incorporpated manually into your own remark template. Alternatively, use the build tool, Backslide.

See [Remark: A simple, in-browser, markdown-driven slideshow tool.](https://github.com/gnab/remark) for full details on remark capabilities. 

## Build with Backslide

Install: `npm install -g backslide` (requires [Node.js](https://nodejs.org/en/))

[Windows users encounering a hummus or python error, check this out](https://github.com/sinedied/backslide/issues/15#issuecomment-334946053)

Create a new directory for your presentation. Copy the files from this template as follows:

```
yourNewPresentation
	|---yourPresentation.md
	|---media/
	\---template/
		|---index.html
		|---style.scss
		\---remark.min.js
```

Modify yourPresentation.md with your slide content. 

Build: From `yourNewPresentation` run `bs e` to build a static HTML file

Serve: `bs s`

See the [Backslide repo page](https://github.com/sinedied/backslide) for more details on building.

## Build the template demo.md
`bs e demo.md -o .`
