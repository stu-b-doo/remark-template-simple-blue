title: Remark template demo

name: readme
class: smalltext
<!-- background-image: url(/media/background_swipe.png) -->
# README

This slide deck was written using [Markdown](https://en.wikipedia.org/wiki/Markdown) and [remark.js](https://remarkjs.com/#1). See the [remark.js wiki](https://github.com/gnab/remark/wiki).

If you've got the HTML version of this presentation:
- To view the presentation open this file in a web browser
  - Press *C* on your keyboard to clone the window for presenting on multiple monitors
  - Press *P* to toggle presenter mode and view slide notes
  - Press *?* to show all keyboard shortcuts

- To edit the presentation open the file in a text editor. Save the file in the editor and refresh the file in the browser to view the changes.
- Only edit the slide content between the `<textarea>` tags.

???
Slide notes beneath the question marks ... :)

---
layout: true
class: normal-background

---
# Presentation
## Author

**Presentation:**

**Author:**

---
# Next slide...

1. Introduction
2. Deep-dive
3. ...

---
class: center, middle, smalltext
# Introduction
This slide has some CSS classes added:

center, middle and smalltext

---
# Inline classes can be added:

.attn[ATTENTION! :)]

.TODO[ this is a todo item... ]

---
# Scale an image
.left-column[
![wikipedia](https://upload.wikimedia.org/wikipedia/en/thumb/8/80/Wikipedia-logo-v2.svg/103px-Wikipedia-logo-v2.svg.png)
]
.right-column[
![:scale 50%;wikipedia](https://upload.wikimedia.org/wikipedia/en/thumb/8/80/Wikipedia-logo-v2.svg/103px-Wikipedia-logo-v2.svg.png)
]

---
class: smalltext
# Tables

Don't write markdown tables by hand. Use a generator [like this one](https://www.tablesgenerator.com/markdown_tables). There are plenty of other table generators, search for them. 

|hello|salfkj|
|--|--|
|sdfasd|sdfasdf|
|biucvb|asdf|
|bcinkjq|w,maerbflh|


Column alignment using `|:---:|`:

|Left alignment|Centre alignment|Right alignment|
|:---|:---:|---:|
|Wow!|Gee|whiz!|


---
class: smalltext
# Columns
.left-column[
This text has the `left-column` css class
- I wandered
- lonely as a
- cloud
]
.right-column[
while this stuff has the `right-column` class
- That floats on high
- o'er vales and hills
]

---
class: split-40

# Variable width columns

See [this issue](https://github.com/gnab/remark/issues/236#issuecomment-108211948).

CSS:
```/*Columns*/
.column:first-of-type {float:left}
.column:last-of-type {float:right}
.split-40 .column:first-of-type {width: 40%}
.split-40 .column:last-of-type {width: 60%}```

.column[
left content
]
.column[
right content
]
    
